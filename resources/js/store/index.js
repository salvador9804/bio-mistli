import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

//'data' en los componentes ya no va a ser necesario, pues todo lo vamos
//a centralizar en el store de Vuex

export default new Vuex.Store({
    state: {
        csrf: false,
        usuario: false,
    },
    mutations: {
        CSRF(state, csrf){
            state.csrf = csrf
        },
        USUARIO(state, usuario){
            state.usuario = usuario;
        },
    }
});
