import Vue from 'vue';
import VueRouter from 'vue-router';
import VuePageTransition from 'vue-page-transition';
import Register from '../components/Register';
import Login from '../components/Login';
import Welcome from '../components/Welcome';
import Tienda from '../components/Tienda';
import Promociones from '../components/Promociones';


Vue.use(VueRouter);
Vue.use(VuePageTransition);

const routes = [
    {path: '/', name: 'welcome', component: Welcome},
    {path: '/registro', name: 'register', component: Register},
    {path: '/login', name: 'login', component: Login},
    {path: '/tienda', name: 'tienda', component: Tienda},
    {path: '/promociones', name: 'promociones', component: Promociones},
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
