<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Welcome
Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [HomeController::class, 'index'])->name('register');

Route::get('/login', [HomeController::class, 'index'])->name('login');

Route::get('/tienda', [HomeController::class, 'index'])->name('tienda');

Route::get('/promociones', [HomeController::class, 'index'])->name('promociones');
